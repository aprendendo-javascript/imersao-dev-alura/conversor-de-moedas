const mainApp = require('../public/scripts/app');

test('Teste data válida', () => {
  const date = {
    day: 12,
    month: 8,
    year: 2020
  };
  expect(mainApp.buildDateString(date)).toBe(`${date.month}-${date.day}-${date.year}`);
});