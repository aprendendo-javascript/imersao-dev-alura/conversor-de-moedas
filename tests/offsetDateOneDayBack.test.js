const mainApp = require('../public/scripts/app');

test('Teste dia de dia qualquer', () => {
  const date = {
    day: 12,
    month: 8,
    year: 2020
  };
  expect(mainApp.offsetDateOneDayBack(date)).toEqual({
    day: 11,
    month: 8,
    year: 2020
  });
});

test('Teste primeiro dia do mês com anterior terminando em 30', () => {
  const date = {
    day: 1,
    month: 5,
    year: 2021
  };
  expect(mainApp.offsetDateOneDayBack(date)).toEqual({
    day: 30,
    month: 4,
    year: 2021
  });
});

test('Teste primeiro dia do mês com anterior terminando em 31', () => {
  const date = {
    day: 1,
    month: 4,
    year: 2021
  };
  expect(mainApp.offsetDateOneDayBack(date)).toEqual({
    day: 31,
    month: 3,
    year: 2021
  });
});

test('Teste primeiro dia do ano', () => {
  const date = {
    day: 1,
    month: 1,
    year: 2021
  };
  expect(mainApp.offsetDateOneDayBack(date)).toEqual({
    day: 31,
    month: 12,
    year: 2020
  });
});

test('Teste primeiro de março de ano bissexto', () => {
  const date = {
    day: 1,
    month: 3,
    year: 2020
  };
  expect(mainApp.offsetDateOneDayBack(date)).toEqual({
    day: 29,
    month: 2,
    year: 2020
  });
});

test('Teste primeiro de março de ano não bissexto', () => {
  const date = {
    day: 1,
    month: 3,
    year: 2018
  };
  expect(mainApp.offsetDateOneDayBack(date)).toEqual({
    day: 28,
    month: 2,
    year: 2018
  });
});