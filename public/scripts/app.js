"use strict"

//HTML Elements
const originalValueField = document.querySelector("#original-value");
const convertedValueField = document.querySelector("#converted-value");
const dateHourContationLabel = document.querySelector("#date-hour-contation-label");

//Code
let currentDayString;
let usdQuote;
let currentDay = getTodayInfo();

do {
  currentDayString = buildDateString(currentDay);
  usdQuote = requestQuotation("USD", currentDayString);
  if (usdQuote === undefined) {
    currentDay = offsetDateOneDayBack(currentDay);
  }
} while (usdQuote === undefined)

const gbpQuote = requestQuotation("GBP", currentDayString);

updateConversionOperation(document.querySelector('[name="currency-radio-selectors"]:checked'));

if (dateHourContationLabel !== null) {
  dateHourContationLabel.innerHTML = "Data/Hora Cotação do Dolar: " + usdQuote.dataHoraCotacao;
}

//Functions
function getTodayInfo() {
  const today = new Date();
  return {
    day: String(today.getDate()).padStart(2, '0'),
    month: String(today.getMonth() + 1).padStart(2, '0'),
    year: today.getFullYear()
  }
}

function buildDateString(date) {
  return `${date.month}-${date.day}-${date.year}`;
}

function offsetDateOneDayBack(date) {
  if (date.day != 1) {
    date.day--;
  } else {
    if ([1, 2, 4, 6, 9, 11].includes(date.month)) {
      date.day = 31;
    } else if (date.month == 3) {
      if (date.year % 4 == 0) {
        if (date.year % 100 == 0) {
          if (date.year % 400 == 0) {
            date.day = 29;
          } else {
            date.day = 28;
          }
        } else {
          date.day = 29;
        }
      } else {
        date.day = 28;
      }
    } else {
      date.day = 30;
    }
    if (date.month != 1) {
      date.month--;
    } else {
      date.month = 12;
      date.year--;
    }
  }
  return date;
}

function requestQuotation(currency, dayString) {
  let quote;
  let urlToRequest = `https://olinda.bcb.gov.br/olinda/servico/PTAX/versao/v1/odata/CotacaoMoedaDia(moeda=@moeda,dataCotacao=@dataCotacao)?@moeda='${currency}'&@dataCotacao='${dayString}'&$top=100&$format=json`;

  let request = new XMLHttpRequest();
  request.open('GET', urlToRequest, false);

  request.onload = function () {
    if (request.readyState == 4 && request.status == 200) { //Sucesso
      let jsonResponse = JSON.parse(request.responseText);
      quote = jsonResponse.value[jsonResponse.value.length - 1];
    }
  };
  request.onerror = function () {
    console.log("Erro:" + request);
  };
  request.send();

  return quote;
}

function updateCovertedField(convertedValue) {
  if (originalValueField.value != "") {
    convertedValueField.value = convertedValue.toFixed(2);
  } else {
    convertedValueField.value = "";
  }
}

function updateConversionOperation(radio) {
  if (radio !== null) {
    if (radio.checked) {
      switch (radio.id) {
        case "USD-BRL":
          updateCovertedField(originalValueField.value * usdQuote.cotacaoVenda);
          originalValueField.oninput = () => updateCovertedField(originalValueField.value * usdQuote.cotacaoVenda);
          break;
        case "BRL-USD":
          updateCovertedField(originalValueField.value / usdQuote.cotacaoVenda);
          originalValueField.oninput = () => updateCovertedField(originalValueField.value / usdQuote.cotacaoVenda);
          break;
        case "GBP-USD":
          updateCovertedField((originalValueField.value * gbpQuote.cotacaoVenda) / usdQuote.cotacaoVenda);
          originalValueField.oninput = () => updateCovertedField((originalValueField.value * gbpQuote.cotacaoVenda) / usdQuote.cotacaoVenda);
          break;
        case "USD-GBP":
          updateCovertedField((originalValueField.value * usdQuote.cotacaoVenda) / gbpQuote.cotacaoVenda);
          originalValueField.oninput = () => updateCovertedField((originalValueField.value * usdQuote.cotacaoVenda) / gbpQuote.cotacaoVenda);
          break;
      }
    }
  }
}

module.exports = {
  buildDateString: buildDateString,
  offsetDateOneDayBack: offsetDateOneDayBack
};