# Conversor de moedas

Conversor simples de moedas, utilizando a API [Dados Abertos do Banco Central](https://dadosabertos.bcb.gov.br/).

## Projeto original: [Codepen Project](https://codepen.io/Kimera21/pen/eYgOXQG)
